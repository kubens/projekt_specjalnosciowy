<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220810083806 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added body type and year of production field';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE car ADD body_type VARCHAR(255) DEFAULT NULL, ADD production_year INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE car DROP body_type, DROP production_year');
    }
}
