<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220803075222 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added relation between RentCar and User';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rent_car ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE rent_car ADD CONSTRAINT FK_77672298A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_77672298A76ED395 ON rent_car (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rent_car DROP FOREIGN KEY FK_77672298A76ED395');
        $this->addSql('DROP INDEX IDX_77672298A76ED395 ON rent_car');
        $this->addSql('ALTER TABLE rent_car DROP user_id');
    }
}
