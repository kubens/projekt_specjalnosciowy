<?php

namespace App\Tests\Validator;

use App\Validator\CarConstraint;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;

class CarConstraintTest extends TestCase
{
    public function testConstraintIsOnClass(): void
    {
        $constraint = new CarConstraint();

        $this->assertSame(Constraint::CLASS_CONSTRAINT, $constraint->getTargets());
    }
}
