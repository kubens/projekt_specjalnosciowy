<?php

namespace App\Tests\Validator;

use App\Validator\RentCar;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;

class RentCarTest extends TestCase
{
    public function testConstraintOnClass(): void
    {
        $constraint = new RentCar();

        $this->assertSame(Constraint::CLASS_CONSTRAINT, $constraint->getTargets());
    }
}
