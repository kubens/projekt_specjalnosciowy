<?php

namespace App\Tests\Validator;

use App\Validator\RentCar;
use App\Validator\RentCarValidator;
use DateTime;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class RentCarValidatorTest extends ConstraintValidatorTestCase
{
    public function incorrectRentCar()
    {
        return [
            ['rent'],
            [420],
            [new \stdClass()],
        ];
    }

    public function correctRentCarDates()
    {
        return [
            [new DateTime('now'), new DateTime('tomorrow')],
            [new DateTime('now'), new DateTime('+10 days')],
        ];
    }

    public function invalidRentCarDates()
    {
        return [
            [new DateTime('now'), new DateTime('yesterday')],
            [new DateTime('tomorrow'), new DateTime('yesterday')],
            [new DateTime('yesterday'), new DateTime('yesterday')],
        ];
    }

    protected function createValidator()
    {
        return new RentCarValidator();
    }

    /**
     * @dataProvider incorrectRentCar
     */
    public function testThrowExceptionOnInvalidRentCarType($rentCar): void
    {
        $constraint = new RentCar();

        $this->expectException(\Exception::class);

        $this->validator->validate($rentCar, $constraint);
    }

    /**
     * @dataProvider correctRentCarDates
     */
    public function testNotRaiseErrorOnCorrectRentCarDates($rentedAt, $returnedAt): void
    {
        $constraint = new RentCar();

        $rentCar = new \App\Entity\RentCar();
        $rentCar->setRentedAt($rentedAt);
        $rentCar->setReturnedAt($returnedAt);

        $this->validator->validate($rentCar, $constraint);

        $this->assertNoViolation();
    }

    /**
     * @dataProvider invalidRentCarDates
     */
    public function testRaiseErrorOnIncorrectRentCarDates($rentedAt, $returnedAt): void
    {
        $constraint = new RentCar();
        $rentCar = new \App\Entity\RentCar();
        $rentCar->setRentedAt($rentedAt);
        $rentCar->setReturnedAt($returnedAt);

        $this->validator->validate($rentCar, $constraint);

        $this->buildViolation($constraint->wrongDatesMessage)->assertRaised();
    }
}
