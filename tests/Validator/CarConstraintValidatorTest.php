<?php

namespace App\Tests\Validator;

use App\Entity\Car;
use App\Validator\CarConstraint;
use App\Validator\CarConstraintValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class CarConstraintValidatorTest extends ConstraintValidatorTestCase
{
    public function validParams(): array
    {
        return [
            // $carId, $costPerMinute
            ['DL-', '40.00'],
            ['DW-', '20.00'],
            ['DW-', '32.00'],
            ['DDL-', '25.00'],
        ];
    }

    public function invalidParams(): array
    {
        return [
            // $carId, $costPerMinute
            ['DL-', '25.00'],
        ];
    }

    public function incorrectCarTypes(): array
    {
        return [
            ['car'],
            [98],
            [98.00],
            [new \stdClass()],
        ];
    }

    protected function createValidator(): CarConstraintValidator
    {
        return new CarConstraintValidator();
    }

    /**
     * @dataProvider incorrectCarTypes
     */
    public function testThrowExceptionOnIncorrectCarTypes($car): void
    {
        $constraint = new CarConstraint();
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Wrong object! You have to pass in an instance of Car');

        $this->validator->validate($car, $constraint);
    }

    /**
     * @dataProvider validParams
     */
    public function testNotRaiseErrorOnProperValues($carId, $costPerMinute): void
    {
        $constraint = new CarConstraint();

        $car = new Car();
        $car->setCarId($carId);
        $car->setCostPerMinute($costPerMinute);

        $this->validator->validate($car, $constraint);

        $this->assertNoViolation();
    }

    /**
     * @dataProvider invalidParams
     */
    public function testRaiseErrorOnIncorrectValues($carId, $costPerMinute): void
    {
        $constraint = new CarConstraint();

        $car = new Car();
        $car->setCarId($carId);
        $car->setCostPerMinute($costPerMinute);

        $this->validator->validate($car, $constraint);

        $this->buildViolation($constraint->message)->atPath('property.path.costPerMinute')->assertRaised();
    }

    public function testRaiseErrorOnIncorrectCarId(): void
    {
        $constraint = new CarConstraint();

        $car = new Car();
        $car->setCarId('DL-');
        $car->setCostPerMinute('20.00');

        $this->validator->validate($car, $constraint);

        $this->buildViolation($constraint->message)->atPath('property.path.costPerMinute')->assertRaised();
    }
}
