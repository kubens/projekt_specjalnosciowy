<?php

namespace App\Tests\Service;

use App\Entity\Car;
use App\Repository\CarRepository;
use App\Service\CarManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CarManagerTest extends TestCase
{
    private $validator;

    private $carRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->validator = $this->createMock(ValidatorInterface::class);
    }

    public function testValidateCorrectCar(): void
    {
        $service = new CarManager($this->validator);
        $car = new Car();

        $errors = $this->createMock(ConstraintViolationListInterface::class);

        $this->validator
            ->expects($this->once())
            ->method('validate')
            ->with($car)
            ->willReturn($errors);


        $service->validateCarObject($car);

        $this->assertCount(0, $errors);
    }
//
//    public function testValidateIncorrectCarThrowsException(): void
//    {
//        $service = new CarManager($this->validator);
//        $car = new Car();
//    }
}
