<?php

namespace App\Tests\Service;

use App\Service\DateOffset;
use PHPUnit\Framework\TestCase;

class DateOffsetTest extends TestCase
{
    public function testGetOffsetDate(): void
    {
        $service = new DateOffset();
        $numberOfDays = 3;
        $direction = false;
        $dateFormat = 'Y-m-d H:i:s';

        $offsetDate = $service->getOffsetDate($numberOfDays, $direction, $dateFormat);
        $offsetDate = new \DateTime($offsetDate);
        sleep(1);
        $expected = new \DateTime('+3 days');

        $diff = $offsetDate->diff($expected, true);

        $this->assertLessThanOrEqual(5, $diff->s);
        $this->assertSame(0, $diff->m);
        $this->assertSame(0, $diff->h);
    }
}
