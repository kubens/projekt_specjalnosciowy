<?php

namespace App\Tests\Entity;

use App\Entity\Car;
use App\Entity\Manufacturer;
use PHPUnit\Framework\TestCase;

class CarTest extends TestCase
{
    public function testGetters(): void
    {
        $costPerMinute = '120.65';
        $carId = 'DW-91234';
        $createdAt = new \DateTime('2022-01-01 12:10:10');
        $updatedAt = new \DateTime('2022-01-01 13:10:10');
        $model = 'Some car model';
        $manufacturer = new Manufacturer();

        $car = new Car();

        $car->setCostPerMinute($costPerMinute)
            ->setCarId($carId)
            ->setCreatedAt($createdAt)
            ->setUpdatedAt($updatedAt)
            ->setManufacturer($manufacturer)
            ->setModel($model)
        ;

        $this->assertSame($costPerMinute, $car->getCostPerMinute());
        $this->assertSame($carId, $car->getCarId());
        $this->assertSame($createdAt, $car->getCreatedAt());
        $this->assertSame($updatedAt, $car->getUpdatedAt());
        $this->assertSame($model, $car->getModel());
        $this->assertSame($manufacturer, $car->getManufacturer());

    }
}
