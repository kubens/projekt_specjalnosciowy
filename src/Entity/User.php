<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'put', 'delete', 'patch'],
    attributes: ['pagination_client_items_per_page' => true],
    denormalizationContext: ['groups' => ['User:write']],
    normalizationContext: ['groups' => ['User:read']]
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'email' => 'ipartial', 'roles' => 'ipartial', 'firstName' => 'ipartial', 'lastName' => 'ipartial'])]
#[ApiFilter(OrderFilter::class, properties: ['id' => 'ASC', 'roles' => 'ASC'])]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['User:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Assert\Email(
        message: 'The email {{ value }} is not a valid email.',
        mode: 'html5',
    )]
    #[Groups(['User:read', 'User:write'])]
    private ?string $email = null;

    #[ORM\Column]
    #[Groups(['User:read', 'User:write'])]
    private array $roles = [];

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\Length(
        min: 2,
        max: 150,
        minMessage: 'First name has to be at least {{ limit }} letters long',
        maxMessage: 'First name can be {{ limit }} letters long'
    )]
    #[Assert\Type(type: ['alpha'])]
    #[Groups(['User:read', 'User:write'])]
    private ?string $firstName = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\Length(
        min: 2,
        max: 150,
        minMessage: 'Last name has to be at least {{ limit }} letters long',
        maxMessage: 'Last name can be {{ limit }} letters long'
    )]
    #[Assert\Type(type: ['alpha'])]
    #[Groups(['User:read', 'User:write'])]
    private ?string $lastName = null;

    #[ORM\Column(length: 255)]
    private ?string $password = null;

    #[Groups(['User:write'])]
    #[SerializedName('password')]
    private ?string $plainPassword = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: RentCar::class)]
    #[ORM\OrderBy(['rentedAt' => 'ASC'])]
    #[Groups(['User:read', 'User:write'])]
    private Collection $rentCars;

    public function __construct()
    {
        $this->rentCars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * This method can be removed in Symfony 6.0 - is not needed for apps that do not check user passwords.
     *
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * This method can be removed in Symfony 6.0 - is not needed for apps that do not check user passwords.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function setPassword(?string $password): User
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection<int, RentCar>
     */
    public function getRentCars(): Collection
    {
        return $this->rentCars;
    }

    public function addRentCar(RentCar $rentCar): self
    {
        if (!$this->rentCars->contains($rentCar)) {
            $this->rentCars->add($rentCar);
            $rentCar->setUser($this);
        }

        return $this;
    }

    public function removeRentCar(RentCar $rentCar): self
    {
        if ($this->rentCars->removeElement($rentCar)) {
            // set the owning side to null (unless already changed)
            if ($rentCar->getUser() === $this) {
                $rentCar->setUser(null);
            }
        }

        return $this;
    }

    #[Groups(['User:read'])]
    public function getFullName(): ?string
    {
        if (null !== $this->getFirstName() && null !== $this->getLastName()) {
            return sprintf('%s %s', $this->getFirstName(), $this->getLastName());
        }

        return null;
    }
}
