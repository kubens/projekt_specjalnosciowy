<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\RentCarRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Validator as CustomAssert;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RentCarRepository::class)]
#[CustomAssert\RentCar]
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'put', 'delete', 'patch'],
    attributes: ['pagination_client_items_per_page' => true],
    denormalizationContext: ['groups' => ['CarRent:write']],
    normalizationContext: ['groups' => ['CarRent:read']]
)]
#[ApiFilter(DateFilter::class, properties: ['rentedAt'])]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'user' => 'ipartial', 'car' => 'ipartial'])]
#[ApiFilter(OrderFilter::class, properties: ['rentedAt' => 'ASC'])]
class RentCar
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['CarRent:read'])]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIMETZ_MUTABLE)]
    #[Groups(['CarRent:read'])]
    private ?\DateTimeInterface $rentedAt = null;

    #[ORM\Column(type: Types::DATETIMETZ_MUTABLE, nullable: true)]
    #[Groups(['CarRent:read'])]
    private ?\DateTimeInterface $returnedAt = null;

    #[ORM\ManyToOne(targetEntity: Car::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['CarRent:read', 'CarRent:write'])]
    private ?Car $car = null;

    #[ORM\ManyToOne(inversedBy: 'rentCars')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['CarRent:read', 'CarRent:write'])]
    private ?User $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRentedAt(): ?\DateTimeInterface
    {
        return $this->rentedAt;
    }

    public function setRentedAt(\DateTimeInterface $rentedAt): self
    {
        $this->rentedAt = $rentedAt;

        return $this;
    }

    public function getReturnedAt(): ?\DateTimeInterface
    {
        return $this->returnedAt;
    }

    public function setReturnedAt(?\DateTimeInterface $returnedAt): self
    {
        $this->returnedAt = $returnedAt;

        return $this;
    }

    public function getCar(): ?Car
    {
        return $this->car;
    }

    public function setCar(?Car $car): self
    {
        $this->car = $car;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
