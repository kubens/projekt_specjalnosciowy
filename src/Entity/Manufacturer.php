<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ManufacturerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ManufacturerRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['Manufacturer:write']],
    normalizationContext: ['groups' => ['Manufacturer:read']]
)]
class Manufacturer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['Manufacturer:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['Manufacturer:read', 'Manufacturer:write'])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'manufacturer', targetEntity: Car::class, cascade: ['remove'])]
    #[Groups(['Manufacturer:read'])]
    private Collection $car;

    public function __construct()
    {
        $this->car = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Car>
     */
    public function getCar(): Collection
    {
        return $this->car;
    }

    public function addCar(Car $car): self
    {
        if (!$this->car->contains($car)) {
            $this->car->add($car);
            $car->setManufacturer($this);
        }

        return $this;
    }

    public function removeCar(Car $car): self
    {
        if ($this->car->removeElement($car)) {
            // set the owning side to null (unless already changed)
            if ($car->getManufacturer() === $this) {
                $car->setManufacturer(null);
            }
        }

        return $this;
    }
}
