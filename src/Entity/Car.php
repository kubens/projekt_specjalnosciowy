<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\CarRepository;
use App\Validator as CarAssert;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\Timestampable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CarRepository::class)]
#[UniqueEntity('carId')]
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get'],
    attributes: ['pagination_client_items_per_page' => true],
    denormalizationContext: ['groups' => ['Car:write']],
    normalizationContext: ['groups' => ['Car:read']]
)]
#[ApiFilter(DateFilter::class, properties: ['createdAt'])]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'carId' => 'ipartial', 'model' => 'ipartial'])]
#[ApiFilter(OrderFilter::class, properties: ['model' => 'ASC', 'createdAt' => 'DESC'])]
class Car
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['Car:read'])]
    private ?int $id = null;

    #[Assert\NotBlank]
    #[Assert\GreaterThan(0)]
    #[ORM\Column(type: Types::DECIMAL, precision: 8, scale: 4)]
    #[Groups(['Car:read', 'Car:write'])]
    private ?string $costPerMinute = null;

    #[CarAssert\RegistrationConstraint(allowNull: true)]
    #[Assert\Length(
        min: 4,
        max: 9,
        minMessage: 'Your car registration number has to be of length at least 4',
        maxMessage: 'Your car registration number can have a maximum length of 9'
    )]
    #[ORM\Column(length: 255)]
    #[Groups(['Car:read', 'Car:write'])]
    private ?string $carId = null;

    #[ORM\Column(type: Types::DATETIMETZ_MUTABLE)]
    #[Timestampable(on: 'create')]
    #[Groups(['Car:read'])]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIMETZ_MUTABLE, nullable: true)]
    #[Timestampable(on: 'update')]
    #[Groups(['Car:read'])]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\ManyToOne(targetEntity: Manufacturer::class, cascade: ["remove"], inversedBy: "car")]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['Car:write', 'Car:read'])]
    private ?Manufacturer $manufacturer = null;

    #[ORM\Column(length: 255)]
    #[Groups(['Car:read', 'Car:write'])]
    private ?string $model = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $bodyType = null;

    #[ORM\Column(nullable: true)]
    private ?int $productionYear = null;

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $model): Car
    {
        $this->model = $model;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): Car
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCostPerMinute(): ?string
    {
        return $this->costPerMinute;
    }

    public function setCostPerMinute(string $costPerMinute): self
    {
        $this->costPerMinute = $costPerMinute;

        return $this;
    }

    public function getCarId(): ?string
    {
        return $this->carId;
    }

    public function setCarId(string $carId): self
    {
        $this->carId = $carId;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getBodyType(): ?string
    {
        return $this->bodyType;
    }

    public function setBodyType(?string $bodyType): self
    {
        $this->bodyType = $bodyType;

        return $this;
    }

    public function getProductionYear(): ?int
    {
        return $this->productionYear;
    }

    public function setProductionYear(?int $productionYear): self
    {
        $this->productionYear = $productionYear;

        return $this;
    }
}
