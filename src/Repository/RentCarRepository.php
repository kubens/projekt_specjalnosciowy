<?php

namespace App\Repository;

use App\Entity\RentCar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RentCar>
 *
 * @method RentCar|null find($id, $lockMode = null, $lockVersion = null)
 * @method RentCar|null findOneBy(array $criteria, array $orderBy = null)
 * @method RentCar[]    findAll()
 * @method RentCar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RentCarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RentCar::class);
    }

    public function add(RentCar $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(RentCar $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
