<?php

namespace App\Repository;

use App\Entity\Car;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Car>
 *
 * @method Car|null find($id, $lockMode = null, $lockVersion = null)
 * @method Car|null findOneBy(array $criteria, array $orderBy = null)
 * @method Car[]    findAll()
 * @method Car[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Car::class);
    }

    public function add(Car $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Car $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByFilters(?int $manufacturerId, ?string $sort): array
    {
        $qb = $this->createQueryBuilder('c');

        if ($manufacturerId) {
            $qb
                ->join('c.manufacturer', 'm')
                ->andWhere('m.id = :manufacturerId')
                ->setParameter('manufacturerId', $manufacturerId);
        }

        if ($sort === 'asc') {
            $qb->orderBy('c.costPerMinute', 'ASC');
        } elseif ($sort === 'desc') {
            $qb->orderBy('c.costPerMinute', 'DESC');
        }

        return $qb->getQuery()->getResult();
    }

    public function findByRegistration(?string $carId, int $page, int $limit, string $sortByDate = 'ASC'): array
    {
        $qb = $this->createQueryBuilder('c');
        $qb->select();
        if (null !== $carId) {
            $qb
                ->where('c.carId LIKE :carId')
                ->setParameter('carId', sprintf('%%%s%%', $carId));
        }

        if ($page > 0) {
            $qb
                ->setMaxResults($limit)
                ->setFirstResult($page * $limit - $limit);
        }

        $qb->orderBy('c.createdAt', $sortByDate);

        return $qb->getQuery()->execute();
    }

    public function findWithPagination(int $page, int $limit): array
    {
        return $this->findByRegistration(null, $page, $limit);
    }

    public function getCarsToUpdate(): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.manufacturer IS NOT NULL')
            ->andWhere('c.model IS NOT NULL')
            ->andWhere('c.bodyType IS NULL')
            ->andWhere('c.productionYear IS NULL')
            ->getQuery()
            ->getResult()
        ;
    }
}
