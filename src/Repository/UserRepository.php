<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(private ValidatorInterface $validator, ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function add(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByEmail(?string $email, int $page, int $limit): array
    {
        $qb = $this->createQueryBuilder('u');
        $qb->select();
        if ('' !== $email) {
            $qb
                ->where('u.email LIKE :email')
                ->setParameter('email', sprintf('%%%s%%', $email));
        }

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($page * $limit - $limit);

        return $qb->getQuery()->execute();
    }

    /**
     * @throws \Exception
     */
    public function validateUserInfo(User $user): void
    {
        $errors = $this->validator->validate($user);
        if (count($errors)) {
            $errorsString = (string) $errors;
            throw new \Exception($errorsString);
        }
    }
}
