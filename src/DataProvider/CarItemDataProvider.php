<?php

namespace App\DataProvider;

use App\Entity\Car;
use App\Repository\CarRepository;

class CarItemDataProvider
{
    public function __construct(private CarRepository $carRepository)
    {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Car::class === $resourceClass;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Car
    {
        return $this->carRepository->find(84);
    }
}