<?php

namespace App\Validator;

use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class RentCarValidator extends ConstraintValidator
{
    /**
     * @throws  Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof RentCar) {
            throw new Exception('Wrong constraint type');
        }

        if (!$value instanceof \App\Entity\RentCar) {
            throw new Exception('Wrong object instance');

        }

        if (false === $this->validateRentDates($value)) {
            $this->context->buildViolation($constraint->wrongDatesMessage)->addViolation();
        }

    }

    private function validateRentDates(\App\Entity\RentCar $rentCar): bool
    {
        $rentedAt = $rentCar->getRentedAt();
        $returnedAt = $rentCar->getReturnedAt();

        return $rentedAt < $returnedAt;
    }
}
