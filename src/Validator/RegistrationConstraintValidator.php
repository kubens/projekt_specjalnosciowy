<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class RegistrationConstraintValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof RegistrationConstraint) {
            throw new UnexpectedTypeException($constraint, RegistrationConstraint::class);
        }

        if (true === $constraint->allowNull && null === $value) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }

        if (!preg_match('/[a-z]{2,3}-\d{1,5}/i', $value)) {
            $this->context->buildViolation($constraint->message)
                 ->setParameter('{{ carId }}', $value)
                 ->addViolation();
        }
    }
}
