<?php

namespace App\Validator;

use App\Entity\Car;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CarConstraintValidator extends ConstraintValidator
{
    /**
     * @throws Exception
     *
     * @var Car ;
     */
    public function validate($car, Constraint $constraint): void
    {
        if (!$car instanceof Car) {
            throw new \Exception('Wrong object! You have to pass in an instance of Car');
        }
        if (str_starts_with($car->getCarId(), 'DL-') && $car->getCostPerMinute() < 30) {
            $this->context->buildViolation($constraint->message)
                 ->atPath('costPerMinute')
                 ->addViolation();
        }
    }
}
