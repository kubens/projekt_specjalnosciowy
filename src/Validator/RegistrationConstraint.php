<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class RegistrationConstraint extends Constraint
{
    public string $message =
        'The registration number "{{ carId }}" contains illegal characters';

    public $allowNull = false;

    public function __construct(bool $allowNull = false, array $options = [], array $groups = null, $payload = null)
    {
        parent::__construct($options, $groups, $payload);

        $this->allowNull = $allowNull;
    }
}
