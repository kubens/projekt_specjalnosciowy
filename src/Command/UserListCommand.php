<?php

namespace App\Command;

use App\Repository\UserRepository;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:user:list',
    description: 'Lists all users',
)]
class UserListCommand extends Command
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository, string $name = null)
    {
        parent::__construct($name);
        $this->userRepository = $userRepository;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::OPTIONAL, 'Email')
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Show limit', 100)
            ->addOption('page', 'p', InputOption::VALUE_OPTIONAL, 'Number of pages', 1)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $section = $output->section();
        $email = (string) $input->getArgument('email');
        $page = $input->getOption('page');
        $limit = $input->getOption('limit');

        $queryResultArray = $this->userRepository->findByEmail($email, $page, $limit);

        $table = new Table($section);
        $table->setHeaders(['ID', 'EMAIL', 'ROLES', 'FIRST NAME', 'LAST NAME']);

        foreach ($queryResultArray as $result) {
            $table->addRow([$result->getId(), $result->getEmail(), implode('', $result->getRoles()), $result->getFirstName(), $result->getLastName()]);
        }

        $table->setStyle('box-double');
        $table->render();

        return Command::SUCCESS;
    }
}
