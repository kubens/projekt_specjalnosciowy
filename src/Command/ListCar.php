<?php

namespace App\Command;

use App\Repository\CarRepository;
use App\Service\CarManager;
use DateTimeInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ListCar extends Command
{
    protected static $defaultName = 'app:car:list';

    public function __construct(private CarRepository $carRepository, private CarManager $carManager, string $name = null)
    {
        parent::__construct($name);
    }

    public function configure()
    {
        $this->addArgument('carId', InputArgument::OPTIONAL);
        // option limit - limit 100
        $this->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Show limit', 100);
        // option page - page strona 1 domyslnie
        $this->addOption('page', 'p', InputOption::VALUE_OPTIONAL, 'Number of pages', 1);
        $this->addOption('sortByDate', 's', InputOption::VALUE_OPTIONAL, 'Sort by date asc or dsc', 'ASC');
        $this->setHelp('This command lists all cars in the database,
         optionally you can enter an argument which will show cars containing entered argument');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $section = $output->section();
        $carId = trim((string) $input->getArgument('carId'));
        $page = $input->getOption('page');
        $limit = $input->getOption('limit');
        $sortByDate = $input->getOption('sortByDate');

        $queryResultArray = $this->carRepository->findByRegistration($carId, $page, $limit, $sortByDate);

        $table = new Table($section);
        $table->setHeaders(['ID', 'COST PER MINUTE', 'REGISTRATION NUMBER', 'CREATED AT']);
        foreach ($queryResultArray as $result) {

            $table->addRow([$result->getId(), $result->getCostPerMinute(), $result->getCarId(), $result->getCreatedAt()->format(DateTimeInterface::ATOM)]);
        }

        $table->setStyle('box-double');
        $table->render();

        return Command::SUCCESS;
    }
}
