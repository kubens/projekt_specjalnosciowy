<?php

namespace App\Command;

use App\Service\Exception\CarNotFoundException;
use App\Service\Exception\UserNotFoundException;
use App\Service\RentManager;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:car:rent',
    description: 'Rent a car for user',
)]
class CarRentCommand extends Command
{
    public function __construct(private RentManager $rentManager, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('carId', InputArgument::REQUIRED, 'CAR ID')
            ->addArgument('rentedAt', InputArgument::REQUIRED, 'RENT DATE')
            ->addArgument('returnedAt', InputArgument::REQUIRED, 'RETURN DATE')
            ->addArgument('email', InputArgument::REQUIRED, 'USER EMAIL')
        ;
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $carId = $input->getArgument('carId');
        $rentedAt = $input->getArgument('rentedAt');
        $returnedAt = $input->getArgument('returnedAt');
        $email = $input->getArgument('email');

        $rentedAt = new \DateTime($rentedAt);
        $returnedAt = new \DateTime($returnedAt);

        try {
            $this->rentManager->rentCar((int) $carId, $rentedAt, $returnedAt, $email);
        } catch (CarNotFoundException|UserNotFoundException $e) {
            $io->error('Error while creating car rent: '.$e->getMessage());

            return Command::FAILURE;
        }
        $io->success('You have rented a car from terminal!');

        return Command::SUCCESS;
    }
}
