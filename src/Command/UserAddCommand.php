<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:user:add',
    description: 'Add new Entity User to database',
)]
class UserAddCommand extends Command
{

    public function __construct(private UserPasswordHasherInterface $userPasswordHasher, private UserRepository $userRepository, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'User email')
            ->addArgument('password', InputArgument::REQUIRED, 'User plain password')
            ->addOption('firstName', null, InputOption::VALUE_REQUIRED, 'User first name')
            ->addOption('lastName', null, InputOption::VALUE_REQUIRED, 'User last name')
            ->addOption('role', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'User role')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');
        $firstName = $input->getOption('firstName');
        $lastName = $input->getOption('lastName');
        $role = $input->getOption('role');

        $user = new User();
        $user->setEmail($email)
             ->setFirstName($firstName)
             ->setLastName($lastName)
             ->setPlainPassword($password)
             ->setRoles($role);

        $hashedPassword = $this->userPasswordHasher->hashPassword($user, $user->getPlainPassword());
        $user->setPassword($hashedPassword);

        try {
            $this->userRepository->validateUserInfo($user);
        } catch (\Exception $exception) {
            $output->writeln(sprintf('<error>%s</error>', $exception->getMessage()));
        }
        $this->userRepository->add($user, true);

        $io->success(
            sprintf(
                'Successfully added new user with email address: %s, firstName: %s, lastName: %s',
                $email,
                $firstName,
                $lastName
            )
        );

        return Command::SUCCESS;
    }
}
