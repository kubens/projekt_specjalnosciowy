<?php

namespace App\Command;

use App\Repository\CarRepository;
use App\Service\CarManager;
use DateTimeInterface;
use Symfony\Component\Console\Helper\Table;
use PHPUnit\Util\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class RemoveCar extends Command
{
    protected static $defaultName = 'app:car:remove';

    public function __construct(private CarRepository $carRepository, private CarManager $carManager, string $name = null)
    {
        parent::__construct($name);
    }

    public function configure()
    {
        $this->setHelp('This command removes a car from the database by given registration number');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $section1 = $output->section();

        $question = new Question('Please enter the car registration number you want to remove: ');
        $helper = $this->getHelper('question');
        $carId = $helper->ask($input, $output, $question);
        $car = $this->carRepository->findOneBy(['carId' => $carId]);

        if ($this->carManager->validateCarObject($car)) {
            $io = new SymfonyStyle($input, $output);
            $io->title('Remove Car');
            $table = new Table($section1);
            $table->setHeaders(['ID', 'COST PER MINUTE', 'REGISTRATION NUMBER', 'CREATED AT'])
                  ->addRow([$car->getId(), $car->getCostPerMinute(), $car->getCarId(), $car->getCreatedAt()->format(DateTimeInterface::ATOM)])
                  ->render();
            $confirmationQuestion = new ChoiceQuestion(
                'Are you sure you want to remove this car from the database?',
                ['yes', 'no']
            );
            $answer = $helper->ask($input, $output, $confirmationQuestion);
            if ('yes' == $answer) {
                $this->carRepository->remove($car, true);
                $output->writeln(printf('Registration number of the car you are removing: %s', $carId));
                $io->success('Successfully removed car from database');
            } else {
                $output->write('Remove car canceled');
            }
        } else {
            throw new Exception('You have entered a registration number that is not inside the database');
        }

        return Command::SUCCESS;
    }
}
