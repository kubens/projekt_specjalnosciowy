<?php

namespace App\Command;

use App\Repository\CarRepository;
use App\Service\CarDataClient\Cars;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:car:updateFromApi',
    description: 'Update missing data from Car API',
)]
class CarUpdateFromApiCommand extends Command
{
    public function __construct(private CarRepository $carRepository, private Cars $cars, string $name = null)
    {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $cars = $this->carRepository->getCarsToUpdate();

        $io->info('Cars to update: '.count($cars));

        foreach ($cars as $car) {
            if (null === $car->getManufacturer() || null === $car->getModel()) {
                continue;
            }
            $carDataFromApi = $this->cars->getCarByModel($car->getManufacturer()->getName(), $car->getModel());

            $bodyType = $carDataFromApi['type'] ?? null;
            $productionYear = isset($carDataFromApi['year']) ? (int) $carDataFromApi['year'] : null;

            if (null !== $bodyType) {
                $io->info(
                    sprintf(
                        'Updating car: %d %s %s | body type: %s | year of production: %d',
                        $car->getId(),
                        $car->getManufacturer()->getName(),
                        $car->getModel(),
                        $bodyType,
                        $productionYear
                    )
                );
            }

            $car->setBodyType($bodyType);
            $car->setProductionYear($productionYear);

            $this->carRepository->add($car, true);

            sleep(1);
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
