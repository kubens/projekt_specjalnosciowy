<?php

namespace App\Command;

use App\Service\DateOffset;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ShowDate extends Command
{
    protected static $defaultName = 'app:show-date';

    public function __construct(private DateOffset $dateOffset, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->addArgument('offset', InputArgument::REQUIRED, 'The number of days to offset');
        $this->setHelp('This command shows you the current date, then the number of days by which you want to offset the date and at the bottom the offset date');
        $this->addOption(
            'backward',
            'b',
            InputOption::VALUE_NONE,
            'Offsets the date forward'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $section1 = $output->section();
        $date = date('l jS \of F Y h:i:s A');

        $section1->writeln($date);

        $offset = (int) $input->getArgument('offset');
        $optionValue = $input->getOption('backward');
        $output->writeln('Offset by days: '.$offset);
        $offsetDate = $this->dateOffset->getOffsetDate($offset, $optionValue, 'l jS \of F Y h:i:s A');
        $output->writeln('Offset date: '.$offsetDate);

        return Command::SUCCESS;
    }
}
