<?php

namespace App\Command;

use App\Entity\Car;
use App\Repository\CarRepository;
use App\Service\CarManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class AddCar extends Command
{
    protected static $defaultName = 'app:car:add';

    public function __construct(private LoggerInterface $logger, private CarRepository $carRepository, private CarManager $carManager, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setHelp('This command adds a new car to the car_rent database');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $verbosityLevelMap = [
            LogLevel::NOTICE => OutputInterface::VERBOSITY_NORMAL,
            LogLevel::INFO => OutputInterface::VERBOSITY_NORMAL,
        ];

        $logger = new ConsoleLogger($output, $verbosityLevelMap);

        $logger->debug('Initializing new Car object');

        $car = new Car();

        $section1 = $output->section();

        $question = new Question('Please enter the car registration number you want to add: ');
        $carId = $this->getHelper('question')->ask($input, $output, $question);
        $logger->debug('Asking user for car registration number and assigning the value to $carId');
        $question2 = new Question('Please enter the cost per minute you want to add: ');
        $costPerMinute = $this->getHelper('question')->ask($input, $output, $question2);
        $costPerMinute = round($costPerMinute, 4);
        $logger->debug('Asking user for cost per minute and assigning the value to $costPerMinute + rounding the value');

        $car->setCarId(strtoupper($carId))
            ->setCostPerMinute($costPerMinute)
            ->setCreatedAt(new \DateTime());
        $logger->debug('Setting car properties with values passed in by the user except CreatedAt which is set manually with new DateTime');
        try {
            $this->carManager->validateCarObject($car);
            $logger->debug('Making sure the car object with users arguments is valid');
        } catch (\Exception $exception) {
            $section1->writeln(sprintf('<error>%s</error>', $exception->getMessage()));
            $logger->debug('Handling the validate exception');
        }

        $this->carRepository->add($car, true);
        $logger->debug('Adding new Car object to database');
        $io = new SymfonyStyle($input, $output);
        $io->title('Add new Car');
        $section1->writeln('Information about added car:');
        $output->writeln(printf('CarId of the car you are adding: %s', $carId));
        $output->writeln(printf('Cost per minute of the car being added: %.4f', $costPerMinute));
        $io->success('Car Successfully added');

        return Command::SUCCESS;
    }
}
