<?php

namespace App\Command;

use App\Repository\CarRepository;
use PHPUnit\Util\Exception;
use App\Service\CarManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateCar extends Command
{
    // komenda do usuwania na podstawie numeru rejestracyjnego
    protected static $defaultName = 'app:car:update';

    public function __construct(private CarRepository $carRepository, private CarManager $carManager, string $name = null)
    {
        parent::__construct($name);
    }

    public function configure()
    {
        $this->setHelp('This command allows you to 
        update price per minute for the chosen car given that the car is already in the database');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $section1 = $output->section();

        $question = new Question('Please enter registration number of the car you want to update: ');
        $carId = $this->getHelper('question')->ask($input, $output, $question);

        $question2 = new Question('Please enter the new cost per minute: ');
        $costPerMinute = $this->getHelper('question')->ask($input, $output, $question2);
        $costPerMinute = round($costPerMinute, 4);
        $car = $this->carRepository->findOneBy(['carId' => $carId]);
        if ($this->carManager->validateCarObject($car)) {
            $car->setCostPerMinute($costPerMinute);
            $io = new SymfonyStyle($input, $output);
            $io->title('Update cost per minute');
            $output->writeln(printf('Registration number of the car which price you are updating: %s', $carId));
            $output->writeln(printf('Price per minute you are setting: %.4f', $costPerMinute));
            $io->success('Price per minute successfully updated');
            $this->carRepository->add($car, true);
        } else {
            throw new Exception('Either the car registration number you entered does not exist 
            or you tried to set the cost per minute to below zero');
        }

        return Command::SUCCESS;
    }
}
