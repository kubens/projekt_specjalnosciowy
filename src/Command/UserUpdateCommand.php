<?php

namespace App\Command;

use App\Repository\UserRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:user:update',
    description: 'Add a short description for your command',
)]
class UserUpdateCommand extends Command
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository, string $name = null)
    {
        parent::__construct($name);
        $this->userRepository = $userRepository;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'Email of user you want to update')
            ->addArgument('password', InputArgument::REQUIRED, 'User password')
            ->addArgument('newEmail', InputArgument::REQUIRED, 'New Email')
            ->addOption('firstName', null, InputOption::VALUE_OPTIONAL, 'New User First Name')
            ->addOption('lastName', null, InputOption::VALUE_OPTIONAL, 'New User Last Name')
            ->addOption(
                'role',
                null,
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'User role',
                []
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');
        $newEmail = $input->getArgument('newEmail');
        $newFirstName = $input->getOption('firstName');
        $newLastName = $input->getOption('lastName');
        $role = $input->getOption('role');

        $user = $this->userRepository->findOneBy(['email' => $email]);

        $user->setEmail($newEmail);
        $user->setFirstName($newFirstName);
        $user->setLastName($newLastName);
        $user->setRoles($role);
        try {
            $this->userRepository->validateUserInfo($user);
        } catch (\Exception $exception) {
            $output->writeln(sprintf('<error>%s</error>', $exception->getMessage()));
        }
        $this->userRepository->add($user, true);

        $io->success(sprintf(
            'Successfully updated user info to: email: %s, firstName: %s, lastName: %s',
            $newEmail,
            $newFirstName,
            $newLastName,
        ));

        return Command::SUCCESS;
    }
}
