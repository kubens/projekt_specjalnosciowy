<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(private UserPasswordHasherInterface $passwordHasher, protected UserRepository $userRepository)
    {
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof User;
    }

    public function persist($data, array $context = [])
    {
        /** @var User $data */
        if (null !== $data->getPlainPassword()) {
            $data->setPassword(
                $this->passwordHasher->hashPassword($data, $data->getPlainPassword())
            );

            $data->eraseCredentials();

            $this->userRepository->add($data, true);
        }
    }

    public function remove($data, array $context = [])
    {
        /** @var User $user */
        $user = $data;
        $this->userRepository->remove($user, true);
    }
}
