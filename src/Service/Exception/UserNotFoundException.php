<?php

namespace App\Service\Exception;

class UserNotFoundException extends \Exception
{
}