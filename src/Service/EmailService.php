<?php

namespace App\Service;

use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class EmailService
{
    private MailerInterface $mailer;
    private Environment $twig;

    public function __construct(MailerInterface $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    /**
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function sendRentNotification($recipient, $rentedAt, $returnDate): void
    {
        $email = (new Email())
            ->from('your_car_rent@example.com')
            ->to($recipient)
            ->subject('Car Rental Notification')
            ->html($this->twig->render('emails/rent_notification.html.twig', [
                'rentedAt' => $rentedAt,
                'returnDate' => $returnDate,
            ]));

        $this->mailer->send($email);
    }
}
