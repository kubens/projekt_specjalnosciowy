<?php

namespace App\Service;

use App\Entity\RentCar;
use App\Entity\User;
use App\Repository\CarRepository;
use App\Repository\RentCarRepository;
use App\Repository\UserRepository;
use App\Service\Exception\CarNotFoundException;
use App\Service\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\Security;

class RentManager
{
    public function __construct(
        private CarRepository $carRepository,
        private Security $security,
        private UserRepository $userRepository,
        private RentCarRepository $rentCarRepository,
        private EmailService $emailService
    ) {
    }

    /**
     * @throws CarNotFoundException
     * @throws UserNotFoundException
     */
    public function rentCar(
        int $carId,
        \DateTimeInterface $rentedAt,
        \DateTimeInterface $returnedAt,
        string $userEmail = null
    ): void {
        $car = $this->carRepository->find($carId);
        if (null === $car) {
            throw new CarNotFoundException();
        }

        if (null === $userEmail) {
            $user = $this->security->getUser();
        } else {
            $user = $this->userRepository->findOneBy(['email' => $userEmail]);
        }

        if (!$user instanceof User) {
            throw new UserNotFoundException();
        }

        $rent = new RentCar();
        $rent
            ->setCar($car)
            ->setUser($user)
            ->setRentedAt($rentedAt)
            ->setReturnedAt($returnedAt);

        $this->rentCarRepository->add($rent, true);

        $userEmail = $user->getEmail();

        $this->emailService->sendRentNotification($userEmail, $rentedAt, $returnedAt);
    }
}
