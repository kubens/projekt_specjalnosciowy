<?php

namespace App\Service\CarDataClient;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Client
{
    private const API_URL = 'https://car-data.p.rapidapi.com/';

    public function __construct(private HttpClientInterface $client, private ParameterBagInterface $bag)
    {
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function get(string $path, array $params): array
    {
        $response = $this->client->request(
            'GET',
            self::API_URL.$path,
            [
                'headers' => [
                    'X-RapidAPI-Host' => 'car-data.p.rapidapi.com',
                    'X-RapidAPI-Key' => $this->bag->get('app.car_data_api.key'),
                ],
                'query' => $params,
            ]
        );

        $statusCode = $response->getStatusCode();
        if (200 !== $statusCode) {
            throw new \Exception('Request failed with status code: '.$statusCode);
        }

        return $response->toArray();
    }
}
