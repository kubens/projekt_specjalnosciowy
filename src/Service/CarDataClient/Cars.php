<?php

namespace App\Service\CarDataClient;

use Psr\Log\LoggerInterface;

class Cars
{
    private const API_PATH = '/cars';

    public function __construct(private Client $client, private LoggerInterface $logger)
    {
    }

    public function getCarByModel(string $manufacturer, string $model): array
    {
        $params = [
            'make' => $manufacturer,
            'model' => $model,
        ];

        try {
            $carData = $this->client->get(self::API_PATH, $params);
        } catch (\Throwable $t) {
            $this->logger->error('Exception while getting car by model', [
                'errorMessage' => $t->getMessage(),
                'params' => $params,
            ]);

            return [];
        }

        $firstKey = array_key_first($carData);


        return (null === $firstKey) ? [] : $carData[$firstKey];
    }
}
