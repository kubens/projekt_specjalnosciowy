<?php

namespace App\Service;

use App\Entity\Car;
use App\Repository\CarRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CarManager
{
    public function __construct(private ValidatorInterface $validator)
    {
    }

    /**
     * @throws \Exception
     */
    public function validateCarObject(Car $car): void
    {
        $errors = $this->validator->validate($car);
        if (count($errors)) {
            $errorsString = (string) $errors;
            throw new \Exception($errorsString);
        }
    }
}
