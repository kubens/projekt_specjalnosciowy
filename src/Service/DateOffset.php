<?php

namespace App\Service;

class DateOffset
{
    public function getOffsetDate(int $numberOfDays, bool $direction, string $dateFormat): string
    {
        $offset = ($numberOfDays * 86400);
        if (true === $direction) {
            $offset *= -1;
        }

        return date($dateFormat, $offset + time());
    }
}
