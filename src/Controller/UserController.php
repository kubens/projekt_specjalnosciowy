<?php

namespace App\Controller;

use App\Entity\Manufacturer;
use App\Entity\RentCar;
use App\Entity\User;
use App\Repository\ManufacturerRepository;
use Doctrine\Common\Collections\Collection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[IsGranted('ROLE_USER')]
class UserController extends AbstractController
{
    #[Route('/user', name: 'app_user')]
    public function index(): Response
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            // TODO
        }

        $rentCars = $this->getRentCars($user);

        return $this->render('user/index.html.twig', [
            'rentCars' => $rentCars,
        ]);
    }

    #[Route('/admin', name: 'app_admin')]
    #[IsGranted('ROLE_ADMIN')]
    public function panelAdmin(ManufacturerRepository $manufacturerRepository): Response
    {
        $user = $this->getUser();
        $manufacturer = $manufacturerRepository->findAll();

        return $this->render('user/admin.html.twig', [
            'manufacturers' => $manufacturer,
        ]);
    }

    #[Route('admin/newmanu', name: 'app_admin_new_manu')]
    public function renderManufacturerForm(Request $request, ManufacturerRepository $manufacturerRepository): Response
    {
        return $this->render('manufacturer/new.html.twig');
    }

    #[Route('admin/addmanu', name: 'app_admin_add_manu')]
    public function addManufacturer(Request $request, ManufacturerRepository $manufacturerRepository): Response
    {
        $name = $request->get('manu-name');
        $manufacturer = new Manufacturer();
        $manufacturer->setName($name);
        $manufacturerRepository->add($manufacturer, true);

        $this->addFlash('success', 'Added new manufacturer');

        return $this->redirectToRoute('app_admin');
    }

    #[Route('/admin/deletemanu/{id}', name: 'app_admin_delete_manu', methods: ['POST'])]
    public function deleteManufacturer(Request $request, Manufacturer $manufacturer, ManufacturerRepository $manufacturerRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$manufacturer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            $cars = $manufacturer->getCar();

            foreach ($cars as $car) {
                // Find the associated RentCar records
                $rentCars = $entityManager->getRepository(RentCar::class)->findBy(['car' => $car]);

                foreach ($rentCars as $rentCar) {
                    $entityManager->remove($rentCar);
                }

                $entityManager->remove($car);
            }

            $entityManager->remove($manufacturer);
            $entityManager->flush();

            $this->addFlash('success', 'Manufacturer deleted successfully');
        }

        return $this->redirectToRoute('app_admin');
    }

    private function getRentCars(User $user): Collection
    {
        return $user->getRentCars();
    }
}
