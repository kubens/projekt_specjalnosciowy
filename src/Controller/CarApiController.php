<?php

namespace App\Controller;

use App\Entity\Car;
use App\Repository\CarRepository;
use App\Repository\ManufacturerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/v0')]
class CarApiController extends AbstractController
{
    private const RECORDS_PER_PAGE = 2;

    public function __construct(private CarRepository $carRepository, private ManufacturerRepository $manufacturerRepository)
    {
    }

    #[Route('/cars/{page}', name: 'app_car_api', methods: 'GET')]
    public function index(?int $page = 0): JsonResponse
    {
        $total = $this->carRepository->count([]);
        $pages = ceil($total / self::RECORDS_PER_PAGE);

        if ($page > $pages) {
            return new JsonResponse([]);
        }

        $cars = $this->carRepository->findWithPagination($page, self::RECORDS_PER_PAGE);

        $return = [
            'currentPage' => $page,
            'count' => count($cars),
            'total' => $total,
            'nextPage' => sprintf('/api/cars/%d', $page + 1),
            'previousPage' => sprintf('/api/cars/%d', $page - 1),
        ];
        foreach ($cars as $car) {
            $return[] = $this->getCarData($car);
        }



        return new JsonResponse($return);
    }

    #[Route('/car', methods: 'POST')]
    public function new(Request $request): JsonResponse
    {
        $carData = [];
        $payload = $request->getContent();
        $newCarData = json_decode($payload, true);

        if (!isset($newCarData['costPerMin']) || !is_string($newCarData['costPerMin'])) {
            throw new UnprocessableEntityHttpException('Missing cost per minute or incorrect type');
        }

        $manufacturer = $this->manufacturerRepository->find($newCarData['manufacturer']);

        $car = new Car();
        $car->setCarId($newCarData['carId'])
            ->setManufacturer($manufacturer)
            ->setModel($newCarData['model'])
            ->setCostPerMinute($newCarData['costPerMin']);

        $this->carRepository->add($car, true);

        return new JsonResponse($this->getCarData($car), 201);
    }

    private function getCarData(Car $car): array
    {
        return [
            'id' => $car->getId(),
            'licensePlate' => $car->getCarId(),
            'costPerMinute' => $car->getCostPerMinute(),
            'createdAt' => $car->getCreatedAt()?->format('Y-m-d H:i:s'),
            'updatedAt' => $car->getUpdatedAt()?->format('Y-m-d H:i:s'),
            'manufacturer' => $car->getManufacturer()?->getName(),
            'model' => $car->getModel(),
        ];
    }
}
