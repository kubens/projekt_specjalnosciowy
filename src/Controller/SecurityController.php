<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Security\RegisterType;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SecurityController extends AbstractController
{
    public function __construct(private UserPasswordHasherInterface $userPasswordHasher, private UserRepository $userRepository, private AuthenticationUtils $authenticationUtils, private Security $security)
    {
    }

    #[Route('/login', name: 'app_login')]
    public function login(): Response
    {
        $error = $this->authenticationUtils->getLastAuthenticationError();

        return $this->render('security/login.html.twig', [
            'error' => $error,
        ]);
    }

    #[Route('/logout', name: 'app_logout')]
    public function logout(): void
    {
    }

    #[Route('/me', name: 'app_me')]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function me(): Response
    {
        $user = $this->security->getToken()->getUser();
        if (!$user instanceof User) {
            throw new \LogicException('Wrong User type');
        }

        return new JsonResponse([
            'userId' => $user->getId(),
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
            'email' => $user->getEmail(),
        ]);
    }

    #[Route('/register', name: 'app_register')]
    public function register(Request $request, ValidatorInterface $validator): Response
    {
        $user = new User();

        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $hashedPassword = $this->userPasswordHasher->hashPassword($user, $user->getPlainPassword());
            $user->setPassword($hashedPassword);
            $errors = $validator->validate($user);

            if (count($errors) > 0) {
                $errorsString = (string) $errors;

                return new Response($errorsString);
            }

            $this->userRepository->add($user, true);

            return $this->redirectToRoute('app_login');
        }

        return $this->renderForm('security/register.html.twig', [
                'form' => $form,
        ]);
    }
}
