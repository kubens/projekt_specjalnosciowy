<?php

namespace App\Controller;

use App\Service\Exception\CarNotFoundException;
use App\Service\Exception\UserNotFoundException;
use App\Service\RentManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RentController extends AbstractController
{
    public function __construct(
        private LoggerInterface $logger,
        private RentManager $rentManager
    ) {
    }

    /**
     * @throws \Exception
     */
    #[Route('/rent/{carId}', name: 'app_rent')]
    public function rent(string $carId, Request $request): Response
    {
        $rentedAt = new \DateTime($request->request->get('rentedAt'));
        $returnedAt = new \DateTime($request->request->get('returnedAt'));

        try {
            $this->rentManager->rentCar($carId, $rentedAt, $returnedAt);
        } catch (CarNotFoundException $e) {
            $this->addFlash('danger', 'Something went wrong. Please try again or contact the administrator');
            $this->logger->critical('Exception while creating car rent: '.$e->getMessage(), [
                'carId' => $carId,
                'userId' => $this->getUser()?->getUserIdentifier(),
                'rentedAt' => $rentedAt,
                'returnedAt' => $returnedAt,
                'errorMessage' => $e->getMessage(),
            ]);

            return $this->redirectToRoute('app_car_index');
        } catch (UserNotFoundException $e) {
            $this->addFlash('danger', 'Something went wrong. Please try again or contact the administrator');
            $this->logger->critical('Exception while creating car rent: '.$e->getMessage(), [
                'carId' => $carId,
                'userId' => $this->getUser()?->getUserIdentifier(),
                'rentedAt' => $rentedAt,
                'returnedAt' => $returnedAt,
                'errorMessage' => $e->getMessage(),
            ]);

            return $this->redirectToRoute('app_login');
        }
        $this->addFlash('success', 'Congratulations! You rented a car.');

        return $this->redirectToRoute('app_user');
    }
}
