<?php

namespace App\Controller;

use App\Service;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    public function __construct(private Service\DateOffset $dateOffset)
    {
    }

    #[Route('/date', name: 'date_offset')]
    public function getDateOffset(Request $request): string
    {
        $response = new Response();

        $numberOfDays = $request->query->get('number', 0);
        $dateFormat = 'l jS \of F Y h:i:s A';
        if ('true' === $request->query->get('direction', 'false')) {
            $direction = true;
        } else {
            $direction = false;
        }
        $offsetDate = $this->dateOffset->getOffsetDate($numberOfDays, $direction, $dateFormat);

        $response->setContent($offsetDate);

        $contents = $this->renderView('index/index.html.twig', [
            'date' => $offsetDate,
        ]);
        $response->setContent($contents);

        return $response->send();
    }
}
