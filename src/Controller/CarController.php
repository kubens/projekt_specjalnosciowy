<?php

namespace App\Controller;

use App\Entity\Car;
use App\Form\CarType;
use App\Repository\CarRepository;
use App\Repository\ManufacturerRepository;
use App\Security\Voter\CarVoter;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Stopwatch\Stopwatch;

#[Route('/car')]
class CarController extends AbstractController
{
    public function __construct(
        private Security $security,
        private LoggerInterface $logger,
        private Stopwatch $stopwatch,
        private ManufacturerRepository $manufacturerRepository
    ) {
    }

    #[Route('/', name: 'app_car_index', methods: ['GET'])]
    public function index(Request $request, CarRepository $carRepository): Response
    {
        // Get the filter values from the request
        $manufacturerId = $request->query->get('manufacturer');
        $sort = $request->query->get('sort');

        // Get the filtered and sorted cars
        $cars = $carRepository->findByFilters($manufacturerId, $sort);

        // Render the car list template or return the updated car list HTML for AJAX request
        if ($request->isXmlHttpRequest()) {
            return $this->render('car/car_rows.html.twig', [
                'cars' => $cars,
            ]);
        }

        // Get the manufacturers for the filter options
        $manufacturers = $this->manufacturerRepository->findAll();

        return $this->render('car/index.html.twig', [
            'cars' => $cars,
            'manufacturers' => $manufacturers,
            'selectedManufacturer' => $manufacturerId,
            'selectedSort' => $sort,
        ]);
    }

    #[Route('/new', name: 'app_car_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CarRepository $carRepository): Response
    {
        $car = new Car();
        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $carRepository->add($car, true);

            return $this->redirectToRoute('app_car_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('car/new.html.twig', [
            'car' => $car,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_car_show', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function show(Car $car): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $this->logger->info('Showing car with id {id}', [
            'id' => $car->getId(),
        ]);

        return $this->render('car/show.html.twig', [
            'car' => $car,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_car_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Car $car, CarRepository $carRepository): Response
    {
        $this->denyAccessUnlessGranted(CarVoter::EDIT, $car);

        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $carRepository->add($car, true);

            return $this->redirectToRoute('app_car_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('car/edit.html.twig', [
            'car' => $car,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_car_delete', methods: ['POST'])]
    public function delete(Request $request, Car $car, CarRepository $carRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$car->getId(), $request->request->get('_token'))) {
            $carRepository->remove($car, true);
        }

        return $this->redirectToRoute('app_car_index', [], Response::HTTP_SEE_OTHER);
    }
}
